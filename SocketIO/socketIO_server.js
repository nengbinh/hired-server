const {ChatModel} = require('../db/models');

module.exports = function (server) {
    const io = require('socket.io')(server);

    // When user create a connection
    io.on('connection', function(socket){
        // Server has receive message from client
        socket.on('sendMsg', function({from, to, content}){
            console.log('get data', {from, to, content});
            const chat_id = [from, to].sort().join('_');
            const creat_time = Date.now();
            new ChatModel({from, to, content, chat_id, creat_time}).save(function(err, chatMsg){
                io.emit('receiveMsg', chatMsg);
            })
        })
        //
    })
};
