var express = require('express');
var router = express.Router();

const md5 = require('blueimp-md5');
const {UserModel, ChatModel} = require('../db/models');
const filter = {password:0, __v:0};

/* Register Router */
router.post('/register', function(req, res) {
  // get body args
  const {username, password, type} = req.body;
  // check if exist
  UserModel.findOne({username}, (err, user)=>{
    if(user) {
      // show error
      res.send({code: 1, msg: 'Username already exist!'});
    }else{
      // create account
      new UserModel({username, type, password: md5(password)}).save((err, user) => {
        // save to cookie
        res.cookie('userid', user._id, {maxAge: 1000*60*60*24});
        const data = {_id: user._id, username, type};
        res.send({code: 0, data});
      });
    }
  });
});

/* Login Router */
router.post('/login', function(req,res){
  // get body args
  const{username, password} = req.body;
  // check if correct
  UserModel.findOne({username, password: md5(password)}, filter, (err, user) => {
    if(user){
      // success
      res.cookie('userid', user._id, {maxAge: 1000*60*60*24});
      res.send({code: 0, user});
    }else{
      // fail
      res.send({code: 1, msg: 'Username or Password incorrect!'});
    }
  })
});

/* Update Router */
router.post('/update', function(req, res){
  const userid = req.cookies.userid;
  if(!userid) {
    return res.send({code: 1, msg: 'Please Login'});
  }
  const user = req.body;
  UserModel.findOneAndUpdate({_id: userid},user, function(err, oldUser){
    if(!oldUser) {
      res.clearCookie('userid');
      res.send({code: 1, msg: 'Please Login'});
    }else{
      const {_id, username, type} = oldUser;
      const data = Object.assign({_id, username, type}, user);
      res.send({code:0, data})
    }
    //
  })
});

/* Get user data based on cookie userid */
router.get('/user', function(req,res){
  const userid = req.cookies.userid;
  if(!userid) {
    return res.send({code: 1, msg: 'Please Login'});
  }
  UserModel.findOne({_id: userid}, filter, function(err, data) {
    res.send({code:0, data});
  })
});

/* Get users list */
router.get('/userlist', function(req,res){
  const {type} = req.query;
  UserModel.find({type, header: {'$exists': true}}, filter, function(err, users){
    res.send({code:0 , data: users})
  })
});

/* Set messages to read */
router.post('/readmsg', function(req,res){
  //
  const from = req.body.from;
  const to = req.cookies.userid;
  //
  ChatModel.update({from, to, read:false},{read: true},{multi: true}, function(err, doc){
    res.send({code:0, data: doc.nModified})
  })
});

/* Get user's chat list */
router.get('/msglist', function(req,res){
  const userid = req.cookies.userid;
  console.log(req.cookies);
  console.log(req);
  UserModel.find(function(err, userDocs){
    const users = {};
    userDocs.forEach(doc => {
      users[doc._id] = {username: doc.username, header: doc.header}
    });
    //
    ChatModel.find({'$or': [{from: userid}, {to: userid}]}, filter, function(err, chatMsgs) {
      res.send({code:0, data: {users, chatMsgs}})
    })
  })
});

module.exports = router;
